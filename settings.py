# This file is part of fastapi_tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from pydantic import BaseSettings


class Settings(BaseSettings):
    tryton_db: str
    tryton_user: int | None = None
    tryton_config: str
