import requests
import simplejson as json

# The port is 5070 by default
api_url = 'localhost:8020'
database = 'DEMO60'

api = '/'.join(['http:/', api_url])
ctx = {
    "company": 1,
    "user": 1,
    "token": "XXXXXXXXXXXXXXXXX",
}


def test_get_countries():
    route = api + '/countries'
    return route, None


def test_write():
    body = {
        'model': 'party.party',
        'context': ctx,
        'ids': [8254],
        'values': {
            'name': 'JAMES BOND PEREZ',
            'code': '007',
            'id_number': '8096251521',
            'type_document': '13',
            'type_person': 'persona_natural',
        },
    }
    route = api + '/write'
    return route, body


def test_create():
    body = {
        'model': 'party.party',
        'context': ctx,
        'record': {
            'name': 'JAMES BOND',
            'code': '007',
            'id_number': '8096251521',
            'type_document': '13',
            'type_person': 'persona_natural',
        },
    }
    route = api + '/create'
    return route, body


def test_search():
    body = {
        'model': 'product.template',
        'context': ctx,
        'domain': [],
        'limit': 5,
    }
    route = api + '/search'
    return route, body


def test_browse():
    body = {
        'model': 'product.template',
        'context': ctx,
        'ids': [15394],
        'fields_names': ['id', 'name', 'code', 'account_category.name'],
    }
    route = api + '/browse'
    return route, body


def test_search_read():
    body = {
        'model': 'product.template',
        'context': ctx,
        'domain': [],
        'limit': 500,
        'fields_names': ['id', 'name', 'code', 'account_category.name'],
    }
    route = api + '/search_read'
    return route, body


def test_delete():
    body = {
        'model': 'party.party',
        'context': ctx,
        'ids': [8254],
    }
    route = api + '/delete'
    return route, body


def test_button_method():
    body = {
        'model': 'sale.sale',
        'context': ctx,
        'method': 'quote',
        'ids': [58310],
    }
    route = api + '/button_method'
    return route, body


def test_report():
    body = {
        'report': 'surveillance.schedule_month.report',
        'context': ctx,
        'data': {
            'period': 12,
            'company': 1,
        },
    }
    route = api + '/report'
    return route, body


def test_wizard():
    body = {
        "context": {
            "company": 1,
            "user": 1,
        },
        "wizard": "staff.payroll_group",
        "method": "transition_open_",
        "view": {
            "start": {
                "period": {
                    "id": 4,
                },
                "department": {
                    "id": 1,
                },
                "description": "PAYROLL DEC. 2022",
                "company": {
                    "id": 1,
                },
                "employees": [{'id': 7}, {'id': 9}],
                "start_extras": None,
                "end_extras": None,
                "wage_types": None,
            },
        },
    }
    route = api + '/wizard'
    return route, body


def test_login():
    body = {
        'model': 'res.user',
        'context': ctx,
        'domain': [
            (),
        ],
        'limit': 5,
        'fields_names': ['id', 'name', 'code'],
    }
    route = api + '/login'
    return route, body


if __name__ == "__main__":
    route, body = test_wizard()
    if body:
        data = json.dumps(body)
        result = requests.post(route, data=data)
    else:
        result = requests.get(route)

    values = result.json()
    if isinstance(values, dict):
        for k, v in values.items():
            print(k, ' : ', v)
    elif isinstance(values, list):
        for v in values:
            print('---------------------------------')
            print(v)
    else:
        print('Response...', values)
