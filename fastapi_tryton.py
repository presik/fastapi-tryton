# This file is part of fastapi_tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import contextvars
import http.client
import logging
import time
from functools import wraps

from starlette.exceptions import HTTPException
from starlette.middleware.base import BaseHTTPMiddleware

try:
    from pydantic import BaseSettings
except Exception:
    from pydantic_settings import BaseSettings

from trytond.config import config
from trytond.exceptions import (
    LoginException,
    MissingDependenciesException,
    RateLimitException,
    TrytonException,
    UserError,
    UserWarning,
)
from trytond.transaction import Transaction, TransactionError

__all__ = ['Tryton']

# init our logger
logger = logging.getLogger("uvicorn.error")
logger.info('Starting FastAPI-Tryton....!')

database_retry = config.getint('database', 'retry')

global _request
_request = contextvars.ContextVar('_request')


def retry_transaction(retry):
    """Decorator to retry a transaction if failed. The decorated method
    will be run retry times in case of DatabaseOperationalError.
    """
    from trytond import backend
    from trytond.transaction import Transaction
    try:
        DatabaseOperationalError = backend.DatabaseOperationalError
    except AttributeError:
        DatabaseOperationalError = backend.get('DatabaseOperationalError')

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for count in range(retry, -1, -1):
                try:
                    return func(*args, **kwargs)
                except DatabaseOperationalError:
                    if count and not Transaction().readonly:
                        continue
                    raise
        return wrapper
    return decorator


class CustomRequestMiddleware(BaseHTTPMiddleware):

    async def dispatch(self, request, call_next):
        _request.set(request)
        response = await call_next(request)
        return response


class Tryton:
    "Control the Tryton integration to one or more FastAPI applications."

    def __init__(self, app=None, configure_jinja=False):
        self.context_callback = None
        self.database_retry = None
        self._configure_jinja = configure_jinja
        if app is not None:
            self.init_app(app)
        app.add_middleware(CustomRequestMiddleware)

    def init_app(self, app):
        "Initialize an application for the use with this Tryton setup."
        database = app.settings.tryton_db
        user = app.settings.tryton_user
        configfile = app.settings.tryton_config
        self.database = database
        config.update_etc(configfile)

        from trytond.pool import Pool
        from trytond.transaction import Transaction
        self.database_retry = config.getint('database', 'retry')
        self.pool = Pool(database)
        self.app = app
        with Transaction().start(database, user, readonly=True):
            self.pool.init()
        if not hasattr(app, 'extensions'):
            self.app.extensions = {}

        self.app.extensions['Tryton'] = self
        if self._configure_jinja:
            app.jinja_env.filters.update(
                numberformat=self.format_number,
                dateformat=self.format_date,
                currencyformat=self.format_currency,
                timedeltaformat=self.format_timedelta,
            )

    def login(self, user, passwd, request):
        from trytond import security
        context = {
            'language': 'es',
            '_request': request,
        }
        parameters = {"device_cookie": None, "password": passwd}
        try:
            session = security.login(
                self.database, user, parameters, context=context)
        except RateLimitException:
            logger.exception('error login')
            raise HTTPException(status_code=http.client.TOO_MANY_REQUESTS, detail='Error to many request for init session') from None
        if not session:
            raise HTTPException(status_code=http.client.UNAUTHORIZED, detail="User name or password incorrect")
        return session

    def logout(self, user, session, request):
        from trytond import security
        security.logout(self.database, user, session, context={'_request': request})

    def _get(self, model, type=None):
        "Return a model instance"
        from trytond.pool import Pool
        model = Pool(self.database).get(model)
        return Model(self.database, model)

    def _get_report(self, name):
        "Return a report instance"
        from trytond.pool import Pool
        report = Pool(self.database).get(name, type='report')
        return Report(self.database, report)

    def _get_wizard(self, name):
        "Return a wizard instance"
        from trytond.pool import Pool
        wizard = Pool(self.database).get(name, type='wizard')
        return Wizard(self.database, wizard)

    def transaction(self, readonly=None, user=None, context=None):
        """Decorator to run inside a Tryton transaction.
        The decorated method could be run multiple times in case of
        database operational error.

        If readonly is None then the transaction will be readonly except for
        PUT, POST, DELETE and PATCH request methods.

        If user is None then tryton_user will be used.

        readonly, user and context can also be callable.
        """
        from trytond import backend
        from trytond.transaction import Transaction

        request = _request.get()
        try:
            DatabaseOperationalError = backend.DatabaseOperationalError
        except AttributeError:
            DatabaseOperationalError = backend.get('DatabaseOperationalError')

        def get_value(value):
            return value() if callable(value) else value

        def instanciate(value):
            if isinstance(value, _BaseProxy):
                return value()
            return value

        def decorator(func):
            @retry_transaction(self.database_retry)
            @wraps(func)
            def wrapper(*args, **kwargs):
                tryton = self.app.extensions['Tryton']
                database = self.app.settings.tryton_db
                if user is None and self.app.settings.tryton_user:
                    transaction_user = get_value(
                        int(self.app.settings.tryton_user),
                    )
                else:
                    transaction_user = get_value(user)

                if readonly is None:
                    is_readonly = tryton._readonly(request)
                else:
                    is_readonly = get_value(readonly)

                transaction_context = {}
                if tryton.context_callback or context:
                    with Transaction().start(database, transaction_user,
                            readonly=True):
                        if tryton.context_callback:
                            transaction_context = tryton.context_callback()
                        transaction_context.update(get_value(context) or {})

                # FIXME
                # is_secure: False
                host, port = request.scope['server']
                transaction_context.setdefault('_request', {}).update({
                    'remote_addr': request.client.host,
                    'http_host': ":".join([host, str(port)]),
                    'scheme': request.url.scheme,
                    'is_secure': False,
                })

                with Transaction().start(database, transaction_user,
                        readonly=is_readonly,
                        context=transaction_context) as transaction:
                    result = {}
                    try:
                        result = func(*map(instanciate, args),
                            **dict((n, instanciate(v))
                                for n, v in kwargs.items()))
                        if hasattr(transaction, 'cursor') and not is_readonly:
                            transaction.cursor.commit()
                    except HTTPException as e:
                        result = e
                    except Exception as e:
                        if isinstance(e, TrytonException):
                            result['error'] = type(e).__name__ + ' ' + str(e)
                            logger.exception(e)
                        elif isinstance(e, Exception):
                            logger.exception(e)
                            result['error'] = type(e).__name__ + ' ' + str(e)
                        raise HTTPException(status_code=http.client.INTERNAL_SERVER_ERROR, detail=result)

                from trytond.worker import run_task
                while transaction.tasks:
                    task_id = transaction.tasks.pop()
                    run_task(tryton.pool, task_id)
                return result
            return wrapper
        return decorator

    def default_context(self, callback):
        "Set the callback for the default transaction context"
        self.context_callback = callback
        return callback

    @property
    def language(self):
        "Return a language instance for the current request"
        from trytond.transaction import Transaction
        Lang = self.pool.get('ir.lang')
        # Do not use Transaction.language as it fallbacks to default language
        language = Transaction().context.get('language')
        if not language:
            #     language = request.accept_languages.best_match(
            #         Lang.get_translatable_languages())
            pass

        # FIXME: add multiple languages
        lang = Lang.get('en')
        return lang

    def format_date(self, value, lang=None, *args, **kwargs):
        from trytond.report import Report
        if lang is None:
            lang = self.language
        return Report.format_date(value, lang, *args, **kwargs)

    def format_number(self, value, lang=None, *args, **kwargs):
        from trytond.report import Report
        if lang is None:
            lang = self.language
        return Report.format_number(value, lang, *args, **kwargs)

    def format_currency(self, value, currency, lang=None, *args, **kwargs):
        from trytond.report import Report
        if lang is None:
            lang = self.language
        return Report.format_currency(value, lang, currency, *args, **kwargs)

    def format_timedelta(
            self, value, converter=None, lang=None, *args, **kwargs):
        from trytond.report import Report
        if not hasattr(Report, 'format_timedelta'):
            return str(value)
        if lang is None:
            lang = self.language
        return Report.format_timedelta(
            value, converter=converter, lang=lang, *args, **kwargs)

    def _readonly(self, request):
        return not (request and request.method in (
            'PUT', 'POST', 'DELETE', 'PATCH'))


class Model:

    def __init__(self, db, model):
        self.db = db
        self.model = model

    def is_readonly(self, request):
        return not (request and request.method in (
            'PUT', 'POST', 'DELETE', 'PATCH'))

    def in_transaction(func):
        "Execute transaction inside a context"
        from trytond.transaction import Transaction

        def instanciate(value):
            if isinstance(value, _BaseProxy):
                return value()
            return value

        def wrapper(*args, **kwargs):
            model, _args = args
            request = _request.get()
            host, port = request.scope['server']
            ctx = _args.pop('context', {})
            user = ctx.get('user', None)
            ctx.setdefault('_request', {}).update({
                'remote_addr': request.client.host,
                'http_host': ":".join([host, str(port)]),
                'scheme': request.url.scheme,
                'is_secure': False,
            })
            is_readonly = model.is_readonly(request)
            retry = config.getint('database', 'retry')
            count = 0
            transaction_extras = {}
            while True:
                if count:
                    time.sleep(0.02 * (retry - count))
                with Transaction().start(model.db, user=user, context=ctx,
                    readonly=is_readonly, **transaction_extras) as transaction:
                    response = {}
                    try:
                        result = func(*map(instanciate, args),
                            **dict((n, instanciate(v))
                                for n, v in kwargs.items()))
                        return result
                    except TransactionError as e:
                        transaction.rollback()
                        transaction.tasks.clear()
                        e.fix(transaction_extras)
                        count += 1
                        continue
                        logger.exception('TransactionError')
                    except HTTPException as err:
                        logger.exception('Error HTTPException')
                        return err
                    except Exception as err:
                        logger.exception('Error Internal Server')
                        if isinstance(err, UserWarning):
                            response['type'] = 'UserWarning'
                            response['error'] = str(err)
                            response['name'] = err.name
                        elif isinstance(err, UserError):
                            response['type'] = 'UserError'
                            response['error'] = str(err)
                            response['domain'] = err.domain
                        elif isinstance(err, LoginException):
                            response['type'] = 'LoginException'
                            response['error'] = err.message
                        elif isinstance(err, MissingDependenciesException):
                            response['type'] = 'MissingDependenciesException'
                            response['error'] = str(err)
                        elif isinstance(err, TrytonException):
                            response['error'] = err.message
                        elif isinstance(err, Exception):
                            response['error'] = type(err).__name__ + ' ' + str(err)
                        raise HTTPException(
                            status_code=http.client.INTERNAL_SERVER_ERROR,
                            detail=response) from err
        return wrapper

    @in_transaction
    def search_read(self, args):
        "Return records from method search_read"
        return self.model.search_read(**args)

    @in_transaction
    def search_count(self, args):
        "Return number records from method search_count"
        return self.model.search_count(**args)

    @in_transaction
    def search(self, args):
        "Return records from method search"
        """
        Method not implemented yet, sorry.
        But the moment isn't possible returns an instance in one request,
        so because hasn't sense return search query, please use 'search_read'
        method instead
        """
        # return self.model.search(**args)
        return []

    @in_transaction
    def browse(self, args):
        "Return records from method browse"
        """
        Method reuse 'search_read', but only with records ids
        """
        fields = args.get('fields_names')
        domain = [('id', 'in', args.get('ids'))]
        return self.model.search_read(domain, fields_names=fields)

    @in_transaction
    def create(self, args):
        "Create a record in Tryton"
        records = [args.get('record')]
        if args.get('records'):
            records = args.get('records')
        result = self.model.create(records)
        return [rec.id for rec in result]

    @in_transaction
    def write(self, args):
        "Write a record in Tryton"
        records = self.model.browse(args['ids'])
        _result = self.model.write(records, args['values'])
        return _result

    @in_transaction
    def delete(self, args):
        "Delete a record in Tryton"
        records = self.model.browse(args['ids'])
        _result = self.model.delete(records)
        return _result

    @in_transaction
    def button_method(self, args):
        "Call a button method in model"
        """
        method: method in model
        args: Dict with args of method

        """
        method = getattr(self.model, args.get('method'))
        records = self.model.browse(args.get('ids', []))
        res = method(records)
        return res

    @in_transaction
    def method(self, data):
        "Call a method in model"
        """
        method: method in model
        args: Dict with args of method

        """
        res = {}
        method = getattr(self.model, data.get('method'))
        _kwargs = data.get('kwargs', {})
        _args = data.get('args')
        if isinstance(_args, dict):
            _args = [_args]
        res = method(*_args, **_kwargs)
        return res

    @in_transaction
    def method_instance(self, args):
        "Call a method in model"
        """
        method: method in model
        args: List with args of method
        kwargs: Dict with args of method
        instance: int or dict
        fields_names: optional

        """
        res = {}
        method = args.get('method')
        args_ = args.get('args', {})
        fields_names = args.get('fields_names', None)
        obj = args.get('instance', None)
        kwargs = args.get('kwargs', {})
        method_ = getattr(self.model, method)
        rpc = None
        if isinstance(obj, int):
            instance = self.model(obj)
        elif isinstance(obj, dict):
            instance = self.model(**obj)
        if method in self.model.__rpc__:
            rpc = self.model.__rpc__[method]
            res = rpc.result(method_(instance, *args_, **kwargs))
        else:
            res = method_(instance, *args_, **kwargs)

        if fields_names and not rpc:
            result = {}
            for f in fields_names:
                result[f] = getattr(res, f)
            res = result
        return res

    @in_transaction
    def fields_get(self, args):
        "Call a all fields in model"
        """
        fields_names: fields in model
        """
        res = self.model.fields_get(args['fields_names'])
        return res


class _BaseProxy:
    pass


class Report:

    def __init__(self, db, report):
        self.db = db
        self.report = report

    def execute(self, records, data, ctx={}):
        """
        # Execute a report

        method: method in model
        args: Dict with args of method

        """
        user = ctx.get('user', None)
        report = {}
        with Transaction().start(
                self.db, user=user, context=ctx, readonly=True):
            try:
                report = self.report.execute(records, data)
            except HTTPException as e:
                result = e
            except Exception as e:
                if isinstance(e, TrytonException):
                    report['error'] = type(e).__name__ + ' ' + str(e)
                    logger.error(e.args)
                    logger.exception(e)
                elif isinstance(e, Exception):
                    # report exception back to server
                    logger.error(str(e))
                    logger.exception(e)
                    report['error'] = type(e).__name__ + ' ' + str(e)
                raise HTTPException(status_code=500, detail=report)
        return report


class ModelAbstract:
    pass


class Wizard:

    def __init__(self, db, wizard):
        from trytond.pool import Pool
        self.db = db
        self.wizard = wizard
        self.pool = Pool(db)

    def run(self, method, view, ctx={}):
        user = ctx.get('user', None)
        with Transaction().start(self.db, user=user, context=ctx):
            session_id, start_state, end_state = self.wizard.create()
            self_wiz = self.wizard(session_id)
            args = view.get(start_state)
            ModelStart = getattr(self_wiz, start_state)
            ModelView = self.pool.get(type(ModelStart).__name__)
            setattr(self_wiz, start_state, ModelView(**args))
            method = getattr(self_wiz, method)
            result = method()
            return result


class Settings(BaseSettings):
    tryton_db: str
    tryton_user: int | None = None
    tryton_config: str
